<?php

namespace console\controllers;
date_default_timezone_set('Asia/Jerusalem');
use common\helpers\CurlHelper;
use common\models\AppSettings;
use common\models\Cron;
use common\models\Log;
use common\models\RunUpdate;
use common\models\SitesApiParams;
use common\models\UserSettings;
use CurlX\RequestInterface;
use frontend\components\ShopifyApi;
use frontend\helpers\Etsy\ProductEtsy;
use frontend\helpers\Html2Text;
use frontend\models\AddProductForm;
use frontend\models\Products;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use yii\console\Controller;
use frontend\models\Variants;
use frontend\helpers\Ebay\Product;
use frontend\helpers\Amazon\AmazonECS;
use yii\db\ActiveQuery;
use yii\db\Exception;
use Yii;
use frontend\helpers\Shopify\ShopifyClient;
use yii\helpers\ArrayHelper;
use frontend\helpers\MailChimp\MailChimpClient;


/**
 * Test controller
 */
class CronController extends Controller {
    private $aliProducts = [];
    private $ebayProducts = [];
    private $walProducts = [];
    private $log;

    public function actionIndex() {
        Yii::$app->cache->flush();
        $this->actionEbay();
        $this->actionAmazon();
        $this->actionAliexpress();
        $this->actionEtsy();
        $this->actionWalmart();
        Yii::$app->cache->flush();
    }

    public function actionEtsy() {
        $lastLog = Log::find()->where(['process' => 'Etsy'])->orderBy('id DESC')->one();

        if($lastLog && !$lastLog->finished && (time()- $lastLog->started) < 3600) {
            return false;
        }
        $db_products = Products::find()->joinWith(['variants'])
            ->andWhere([Products::tableName().'.Vendor' => 'etsy'])->asArray()->all();
        if(!$db_products) return false;

        $log = new Log();
        $log->startProcess('Etsy');

        $productsId = ArrayHelper::getColumn($db_products, 'ID');
        $productCount = count($productsId);
        $etsyKeysCont = count(ArrayHelper::getValue(SitesApiParams::sitesApiParams(), 'etsy'));
        $remains = 0;
        $productsIdCount = $productCount;
        if($productCount > $etsyKeysCont) {
            $remains = $productCount % $etsyKeysCont;
            $productsIdCount = ($productCount - $remains) / $etsyKeysCont;
        }
        foreach (ArrayHelper::getValue(SitesApiParams::sitesApiParams(), 'etsy', []) as $index => $key) {
            $logNew = new log(['started' => time(), 'process' => 'etsy child', 'parent_id' => $log->id, 'api_key' => $index]);
            $logNew->save();
            if($index == $etsyKeysCont -1) {
                $length  = $productsIdCount + $remains;
            } else {
                $length = $productsIdCount;
            }
            $productIds = array_slice($productsId, $index * $productsIdCount, $length);
            Products::updateAll(['log_id' => $logNew->id], [ 'ID' => $productIds]);
        }
        $child_logs = Log::find()->where(['parent_id' => $log->id])->indexBy('id')->all();
        foreach ($child_logs as $id => $child_log) {
            $log->addInfo('etsy id loop');
            $result = Products::find()->where(['Vendor' => 'etsy', 'log_id' => $id])->with('user')->all();
            if(!$result) {
                continue;
            }
            exec('php '.Yii::getAlias('@root').'/yii cron/etsy-product-update '.$id.' > /dev/null 2>/dev/null &');
        }
    }

    public function actionEtsyProductUpdate($id) {
        $log = Log::findOne($id);
        $result = Products::find()->where(['Vendor' => 'etsy', 'log_id' => $id])->with('user')->all();
        if(!$result) {
            return false;
        }

        $etsyItemId = [];
        $log->addInfo('etsy id loop');

        foreach ($result as $product){
            $etsyItemId[] = Log::getParceUrl('etsy', $product['ProductUrl']);
        }
        $log->addInfo('finish');
        $etsyItemId = array_unique($etsyItemId);
        $log->addInfo(count($etsyItemId));
        $productsId = ArrayHelper::getColumn($result, 'ID');
        $variants = Variants::find()->where(['in', 'ProductID', $productsId])->andWhere(['!=', 'SKU', ''])->indexBy('SKU')->all();

        $log->addInfo('loop ids');
        $log->addInfo('update product count => '. count($etsyItemId) );
        foreach ($etsyItemId as $id) {
            usleep(500);
            $log->addInfo($id);
            $productEtsy = new ProductEtsy($log->api_key);
            $productEtsy->itemId = $id;
            $product = $productEtsy->getProduct();
            $product = json_decode($product);

            if(isset($product->results[0]->has_variations) && $product->results[0]->has_variations == true){
                sleep(1);
                $productInventory = $productEtsy->getProductInventory();
                if($productInventory){
                    $product->variants = $productInventory;
                    $res = json_decode($product->variants);
                    if($res)
                        foreach ($res->results->products as $variant) {
                            if (isset($variant->offerings['0']->price->amount)) {
                                $price = $variant->offerings['0']->price->amount / 100;
                            }else {
                                $price = 0;
                            }

                            if(!$shopProductID = ArrayHelper::getValue($variant, 'product_id')) {
                                $shopProductID = ArrayHelper::getValue($variant, 'property_values.0.value_ids.0');
                            }
                            if(!$val = ArrayHelper::getValue($variants, $shopProductID)) {
                                continue;
                            }
                            if ($val['Price'] != $price && $val['SKU'] == $shopProductID && $variant->offerings['0']->quantity) {
                                $val['Price'] = $price;
                                $val['LastScanTime'] = date('y/m/d H:i:s');
                                $val->toBeSynced = 1;
                            }else {
                                $val['LastScanTime'] = date('y/m/d H:i:s');
                            }
                            if ($val['StockUnits'] != $variant->offerings['0']->quantity && $val['SKU'] == $shopProductID) {
                                $val['StockUnits'] = $variant->offerings['0']->quantity;
                                $val->toBeSynced = 1;
                                 $val['LastScanTime'] = date('y/m/d H:i:s');
                            }
                            $val['LastScanTime'] = date('y/m/d H:i:s');
                            $val->save();
                        }
                }
            }else{
                if(empty($variants[$product->params->listing_id])) {
                    continue;
                }
                $val = $variants[$product->params->listing_id];

                if (isset($product->results['0']->price)) {
                    $price = $product->results['0']->price;
                }else {
                    $price = 0;
                }
                if($val['Price'] != $price && $product->results['0']->quantity){
                    $val['Price'] = $price;
                     $val['LastScanTime'] = date('y/m/d H:i:s');
                    $val->toBeSynced = 1;
                }else{
                     $val['LastScanTime'] = date('y/m/d H:i:s');
                }

                if($val['StockUnits'] != $product->results['0']->quantity ) {
                    $val['StockUnits'] = $product->results['0']->quantity;
                    $val->toBeSynced = 1;
                     $val['LastScanTime'] = date('y/m/d H:i:s');
                }
                $val['LastScanTime'] = date('y/m/d H:i:s');
                $val->save();
            }
            ++$log->productCount;
        }
        $log->finished =  time();
        $log->save();
        $childsLog = log::findAll(['parent_id' => $log->parent_id, 'finished' => null]);

        if(!$childsLog) {

            $users_id = ArrayHelper::getColumn($result, 'UserID');
            $users_id = array_unique($users_id);
            if($users_id) {
                $users = UserSettings::find()->where(['IN', 'id', $users_id])->indexBy('id')->asArray()->all();
                $appSetting = AppSettings::find()->one();
                $apiKey = $appSetting->api_key;
                $secret = $appSetting->shared_secret;
                $log->addInfo('etsy user_id loop finish');

                foreach ($users as $user_id => $user){
                    $token = $user['access_token'];
                    $shop = $user['store_name'];
                    $sc = new ShopifyClient($shop, $token, $apiKey, $secret);
                    $log->addInfo('Updating variants');
                     Variants::updateShopifyVariants($sc, $log, $user_id);
                }
            }

            $log->parent->addInfo('etsy user_id loop finish');
            $log->parent->productCount = Log::find()->where(['parent_id' => $log->parent_id])->sum('productCount');
            $log->parent->finished = time();
            $log->parent->save();
        }
        return true;
    }

    public function actionEbay()
    {
        $lastLog = Log::find()->where(['process' => 'Ebay'])->orderBy('id DESC')->one();
        if($lastLog && !$lastLog->finished && (time()- $lastLog->started) < 3600) {
            return false;
        }

        $db_products = Products::find()->joinWith(['variants'])->andWhere(['Vendor' => 'ebay'])->asArray()->all();
        if(!$db_products)
            return false;

        $log = new Log();
        $log->startProcess('Ebay');
        $this->ebayProducts = Products::find()->where(['Vendor' => 'ebay'])->with('variants')->indexBy('ProductUrl')->all();
        $links = array_keys($this->ebayProducts);

        $curl = new CurlHelper([$this, 'ebayFinished'], 10); // call aliFinished callback when one of requests is finished
        foreach ($links as $link) {
            $curl->requests($link);
        }
        $curl->execute();
        $users_id = ArrayHelper::getColumn($db_products, 'UserID');
        $users_id = array_unique($users_id);
        if($users_id) {
            $users = UserSettings::find()->where(['IN', 'id', $users_id])->indexBy('id')->asArray()->all();
            $appSetting = AppSettings::find()->one();
            $apiKey = $appSetting->api_key;
            $secret = $appSetting->shared_secret;
            $log->addInfo('ebay user_id loop');
            $ebayCount = 0;
            foreach ($users as $id => $user){
                $token = $user['access_token'];
                $shop = $user['store_name'];
                $sc = new ShopifyClient($shop, $token, $apiKey, $secret);

                Variants::updateShopifyVariants($sc, $log, $id);
                $log->productCount(++$ebayCount);
            }
        }
        $log->addInfo('ebay user_id loop finish');
        $log->finished = time();
        $log->save();

    }

    public function ebayFinished(RequestInterface $request)
    {
        $link = $request->getUrl();
        $code = $request->http_code;
        $client = $request->response;
        if($code == 429) {
            $f = fopen(Yii::getAlias('@console').'/controllers/crawlera_log.data', 'a');
            $message = "\n\n".date('Y-m-d H:i:s')." Status Code 429 \n
             - request - $link \n
             - response - $client \n\n
             ";
            fputs($f, $message);
            return false;
        }

        if(!is_string($client)) {
            return false;
        }

        $localProduct = ArrayHelper::getValue($this->ebayProducts, str_replace('http:', 'https:', $link));
        /* @var Products $localProduct */
        if(!$localProduct || !$client) {
            return false;
        }

        $product = new Product($client, $link);
        $product = $product->getProduct(false);


        if (isset($product['data']->Item) && isset($product['data']->Item->Variations) && isset($product['data']->Item->Variations->Variation)) {
            foreach ($product['data']->Item->Variations->Variation as $variant) {
                foreach ($localProduct->variants as $val) {
                    $rrr = $val['product']['0']['UserID'];
                    if ($val['Price'] != $variant->StartPrice && $val['SKU'] == $variant->SKU && $variant->Quantity) {
                        $val['Price'] = $variant->StartPrice;
                         $val['LastScanTime'] = date('y/m/d H:i:s');
                        $val->toBeSynced = 1;
                        //$val->save();
                        file_put_contents('/home/ara100/app.inventorify.com/console/controllers/log', $val['ID'] . "\t" . $val['ProductID'] . "\t\t\t\t\t" . $val['SKU'] . "\t\t\t" . $rrr . "\t\t" . $val->product->ProductUrl . "\t\t" . $val['Price'] . "\t\t" . $variant->StartPrice . "\n", FILE_APPEND);
                    } else {
                         $val['LastScanTime'] = date('y/m/d H:i:s');
                        //$val->save();
                        file_put_contents('/home/ara100/app.inventorify.com/console/controllers/log', $val['ID'] . "\t" . $val['ProductID'] . "\t\t\t\t\t" . $val['SKU'] . "\t\t\t" . $rrr . "\t\t" . $val->product->ProductUrl . "\t\t" . $val['Price'] . "\t\t" . $val['Price'] . "\n", FILE_APPEND);
                    }


                    if ($val['StockUnits'] != $variant->Quantity && $val['SKU'] == $variant->SKU) {
                        $val->StockUnits = $variant->Quantity;
                        $val->toBeSynced = 1;
                         $val['LastScanTime'] = date('y/m/d H:i:s');
                        //$val->save();
                    }
                    $val['LastScanTime'] = date('y/m/d H:i:s');
                    $val->save();
                }
            }

        } else {
            foreach ($localProduct->variants as $val) {
                $rrr = $val['product']['0']['UserID'];
                if ($val['Price'] != $product['data']->Item->CurrentPrice && $val['SKU'] == $product['data']->Item->ItemID && $product['data']->Item->Quantity) {
                    $val->Price = $product['data']->Item->CurrentPrice;
                     $val['LastScanTime'] = date('y/m/d H:i:s');
                    $val->toBeSynced = 1;
                    //$val->save();
                    file_put_contents('/home/ara100/app.inventorify.com/console/controllers/log', $val['ID'] . "\t" . $val['ProductID'] . "\t\t\t\t\t" . $val['SKU'] . "\t\t\t" . $rrr . "\t\t" . $val->product->ProductUrl . "\t\t" . $val['Price'] . "\t\t" . $product->results['0']->price . "\n", FILE_APPEND);
                } else {
                     $val['LastScanTime'] = date('y/m/d H:i:s');
                    //$val->save();
                    file_put_contents('/home/ara100/app.inventorify.com/console/controllers/log', $val['ID'] . "\t" . $val['ProductID'] . "\t\t\t\t\t" . $val['SKU'] . "\t\t\t" . $rrr . "\t\t" . $val->product->ProductUrl . "\t\t" . $val['Price'] . "\t\t" . $val['Price'] . "\n", FILE_APPEND);
                }

                if ($val['StockUnits'] != $product['data']->Item->Quantity && $val['SKU'] == $product['data']->Item->ItemID) {
                    $val->StockUnits = $product['data']->Item->Quantity;
                    $val->toBeSynced = 1;
                     $val['LastScanTime'] = date('y/m/d H:i:s');
                    //$val->save();
                }
                $val['LastScanTime'] = date('y/m/d H:i:s');
                $val->save();
            }
        }
    }

    public function actionAmazon() {
        ini_set('memory_limit', '3000M');
        $lastLog = Log::find()->where(['process' => 'Amazon'])->orderBy('id DESC')->one();

        if($lastLog && !$lastLog->finished && (time()- $lastLog->started) < 3600) {
            return false;
        }
        $log = new Log();
        $log->startProcess('Amazon');
        $productsId = Products::find()->where(['Vendor' => 'amazon' ])->select('ID')->column();
        if($productsId) {
            $productCount = count($productsId);
            $amazonKeysCont = count(ArrayHelper::getValue(SitesApiParams::sitesApiParams(), 'amazon'));
            $remains = 0;
            $productsIdCount = $productCount;
            if($productCount > $amazonKeysCont) {
                $remains = $productCount % $amazonKeysCont;
                $productsIdCount = ($productCount - $remains) / $amazonKeysCont;
            }
            foreach (ArrayHelper::getValue(SitesApiParams::sitesApiParams(), 'amazon') as $index => $key) {
//                if(!$index) continue;
                if($index == $amazonKeysCont -1) {
                    $length  = $productsIdCount + $remains;
                } else {
                    $length = $productsIdCount;
                }
                $productIds = array_slice($productsId, $index * $productsIdCount, $length);
                if($productIds) {
                    $logNew = new log(['started' => time(), 'process' => 'amazon child', 'parent_id' => $log->id, 'api_key' => $index]);
                    $logNew->save();
                    Products::updateAll(['log_id' => $logNew->id], [ 'ID' => $productIds]);
                    exec('php '.Yii::getAlias('@root').'/yii cron/amazon-product '.$logNew->id.' > /dev/null 2>/dev/null &');
                }
            }
        }
    }

    public function actionAmazonProduct($id) {
        $log = Log::findOne($id);
        $result = Products::find()->where(['Vendor' => 'amazon', 'log_id' => $id])->with(['user', 'variants'])->indexBy('ID')->all();
        $log->addInfo('product count '. count($result));
        if(!$result) {
            return false;
        }
        $amazonItemId = [];
        $skuMapping = [];
        $amazonRegions = [];
        foreach ($result as $productId => $amazonProduct) {
            /* @var Products $amazonProduct*/
            $itemId = Log::getParceUrl('amazon', $amazonProduct['ProductUrl']);
            $amazonItemId[$productId] = $itemId;
            $country =  Log::getParceUrl('amazon', $amazonProduct['ProductUrl'], true);
            $amazonRegions[$itemId] = $country? $country : $amazonProduct->user->country;
            $skuMapping[$itemId] = $amazonProduct->ID;
        }

        $amazonItemId = array_unique($amazonItemId);

        $productsIds = ArrayHelper::getColumn($result, 'ID');
        $productsIds = array_unique($productsIds);
        $log->addInfo('products Ids =>'. implode(',', $productsIds));
        $variants = Cron::getProductVariants($productsIds);
        $log->addInfo('variants count '. count($variants));
        $log->addInfo('loop ids');

        foreach($amazonItemId as $productId => $id){
            //$log->addInfo('starting '.$id);
           // $log->addInfo('region =>  '.$amazonRegions[$id]);
            $product_start_time = time();
            $reg = $amazonRegions[$id];
            if(!$apiResult = Cron::scrapAmazonApi($id, $reg, $variants, $log)) {
                $dbProduct = $result[$skuMapping[$id]];
                /* @var Products $dbProduct*/
                foreach ($dbProduct->variants as $dbVariant) {
                    $newVariants = Cron::getProductVariants('', $dbVariant);
                //    $log->addInfo('starting product variants '.$dbVariant->SKU);
                    Cron::scrapAmazonApi($dbVariant->SKU, $reg, $newVariants, $log);
                }
            }

            if(ArrayHelper::getValue($apiResult, 'error')) {
                Variants::updateAll(['StockUnits' => 'Out of stock'], ['ProductID' => $productId]);
            }
            if($productsIds) {
                Variants::updateAll(['LastScanTime' => date('y/m/d H:i:s')], ['IN', 'ProductID', $productsIds]);
            }
            ++$log->productCount;
            $processTime = time() - $product_start_time;

            //$log->addInfo("amazon update $id DB ".$processTime);

        }

        unset($result);
/*
        if($variantsUnset && array_filter($variants)) {
            foreach ($variants as $variant) {
                $variant->StockUnits = 'Out of stock';
                $variant->LastScanTime =  date('y/m/d H:i:s');
                $variant->save();
            }
        }*/

        unset($variants);
        if($productsIds) {
            Variants::updateAll(
                ['StockUnits' => 'Out of stock', 'LastScanTime' =>  date('y/m/d H:i:s')],
                ['AND',
                    ['IN', 'ProductID',  $productsIds],
                    ['=', 'SKU', '']
                ]);
        }

        $log->finished =  time();
        $log->save();
        usleep(rand(10, 300));
        $childsLog = log::findAll(['parent_id' => $log->parent_id, 'finished' => null]);
        if(!$childsLog) {
            $db_products = Products::find()->andWhere(['Vendor' => 'amazon'])->asArray()->all();

            $users_id = [];
            foreach ($db_products as $key => $db_product){
                $users_id[$key] = $db_product['UserID'];
            }

            $users_id = array_unique($users_id);

            $appSetting = new AppSettings();
            $res = $appSetting->find()->one();
            $apiKey = $res->api_key;
            $secret = $res->shared_secret;

            $log->addInfo('amazon user_id loop');

            if($users_id) {
                $users = UserSettings::find()->where(['IN', 'id', $users_id])->indexBy('id')->asArray()->all();
                foreach ($users as $user_id =>$user) {
                    $product_start_time = time();
                    //$userSettings = $user[$id]; //@TODO check
                    $userSettings = $user; //@TODO check
                    $token = $userSettings['access_token'];
                    $shop = $userSettings['store_name'];

                    $log->addInfo('shop - ' . $shop);

                    $sc = new ShopifyClient($shop, $token, $apiKey, $secret);

                    Variants::updateShopifyVariants($sc, $log, $user_id);
                    $processTime = time() - $product_start_time;
                    $log->addInfo("amazon update $id Shopify " . $processTime);
                }
            }
            if($log->parent) {
                $log->parent->finished = time();

                $log->parent->productCount = Log::find()->where(['parent_id' => $log->parent_id])->sum('productCount');
                $log->parent->save();
            }
        }
        return true;
    }

    public function actionAliexpress() {
        $lastLog = Log::find()->where(['process' => 'Aliexpress'])->orderBy('id DESC')->one();
        if($lastLog && !$lastLog->finished && (time()- $lastLog->started) < 3600) {
            return false;
        }

        // Sending Curl Paralel requests to aliexpress
        $this->aliProducts = Products::find()->where([
            'Vendor' => 'aliexpress'
        ])->with('variants')->indexBy('ProductUrl')->all();


        if(!$this->aliProducts)
            return false;

        $log = new Log();
        $log->process = 'Aliexpress';
        $log->started = time();
        $log->save();

        /*// Sending Curl Paralel requests to aliexpress
        $this->aliProducts = Products::find()->where([
            'Vendor' => 'aliexpress'
        ])->with('variants')->indexBy('ProductUrl')->all();*/

        if (!file_exists('/home/ara100/app.inventorify.com/console/controllers/log')) {
            fopen('/home/ara100/app.inventorify.com/console/controllers/log', 'a');
            file_put_contents('/home/ara100/app.inventorify.com/console/controllers/log', 'ID' . "\t\t" . 'Product ID' . "\t\t\t" . 'Variant ID' . "\t\t\t" . 'User ID' . "\t\t" . 'URL' . "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" . 'Old values' . "\t" . 'New values' . "\n", FILE_APPEND);
        }

        $log->addInfo('aliexpress url loop');
        $links = array_keys($this->aliProducts);
        $curl = new CurlHelper([$this, 'aliFinished'], 10); // call aliFinished callback when one of requests is finished

        $this->log = $log;
        $aliexpressCount = 0;
        foreach ($links as $link) {
            $link = str_replace('https', 'http', $link);
            $curl->requests($link);
            $log->productCount(++$aliexpressCount);
        }

        $log->addInfo('finish');

        $curl->execute();
        // finish requests



        // Starting shopify
        $db_products = Products::find()->joinWith(['variants'])->andWhere(['Vendor' => 'aliexpress'])->asArray()->all();
        $users_id = [];
        foreach ($db_products as $key => $db_product){
            $users_id[$key] = $db_product['UserID'];
        }

        $users_id = array_unique($users_id);

        $appSetting = new AppSettings();
        $res = $appSetting->find()->one();
        $apiKey = $res->api_key;
        $secret = $res->shared_secret;


        $log->addInfo('aliexpress user_id loop');

        foreach ($users_id as $id){
            $model = new UserSettings();
            $userSettings = $model->findOne($id);

            $token = $userSettings['access_token'];
            $shop = $userSettings['store_name'];
            $sc = new ShopifyClient($shop, $token, $apiKey, $secret);

            Variants::updateShopifyVariants($sc, $log, $id);
        }

        $log->addInfo('aliexpress user_id loop finish');

        $log->finished = time();
        $log->save();
    }

    public function aliFinished(RequestInterface $request)
    {
        $link = $request->getUrl();
        $code = $request->http_code;
        $client = $request->response;
        if($code == 429) {
            $f = fopen(Yii::getAlias('@console').'/controllers/crawlera_log.data', 'a');
            $message = "\n\n".date('Y-m-d H:i:s')." Status Code 429 \n
             - request - $link \n
             - response - $client \n\n
             ";
            fputs($f, $message);
            return false;
        }



        if(!is_string($client)) {
            return false;
        }

        $localProduct = ArrayHelper::getValue($this->aliProducts, str_replace('http:', 'https:', $link));
        /* @var Products $localProduct */
        if(!$localProduct || !$client) {
            return false;
        }

        $dom = new \DOMDocument;

        $client = mb_convert_encoding($client, 'HTML-ENTITIES', "UTF-8");

        //var_dump($client);die;

        libxml_use_internal_errors(true);

        $dom->loadHTML($client);
        foreach ($dom->getElementsByTagName('h1') as $ptag) {
            if ($ptag->getAttribute('class') == "product-name") {
                $title = $ptag->nodeValue;
            }
        }

        foreach ($dom->getElementsByTagName('img') as $ptag) {
            if ($ptag->getAttribute('alt') == $title) {
                $t[] = $ptag->getAttribute('src');
            }
        }

        foreach ($dom->getElementsByTagName('div') as $ptag) {
            if ($ptag->getAttribute('id') == 'j-product-info-sku') {
                $y = $ptag->getElementsByTagName('dl');
            }
        }

        $listsName = [];



        foreach ($dom->getElementsByTagName('dt') as $item) {
            if ($item->getAttribute('class') == 'p-item-title') {
                $listsName[] = $item->nodeValue;
            }
        }

        if(!$listsName && $this->log) {
            $this->log->addInfo($link.' - elements not found');
        }

        $count = $y->length;

        $i = 0;

        foreach ($listsName as $value) {
            if ($i != $count) {
                $lists[] = str_replace(':', '', $value);
            } else {
                break;
            }
            $i++;
        }

        $variantNames = [];

        foreach ($dom->getElementsByTagName('ul') as $key => $item) {
            if ($item->getAttribute('class') == 'sku-attr-list util-clearfix') {
                $variantNames[] = $item;
            }
        }

        $attributes = [];
        if ($variantNames != null) {
            foreach ($variantNames as $l => $value) {
                foreach ($value->childNodes as $key => $val) {
                    $attributes[$l][$key] = $dom->saveHTML($val);
                }
            }

            foreach ($attributes as $l => $attribute) {
                foreach ($attribute as $key => $tag) {
                    if ($tag != '') {
                        $DOM = new \DOMDocument();
                        $DOM->loadHTML($tag);
                        $flag = false;
                        if (!$flag) {
                            foreach ($DOM->getElementsByTagName('a') as $a) {
                                $sku = $a->getAttribute('data-sku-id');

                                foreach ($DOM->getElementsByTagName('span') as $span) {
                                    $listsValue[$l][$sku] = $span->nodeValue;
                                }
                            }
                            $flag = true;
                        }

                        if ($flag) {
                            foreach ($DOM->getElementsByTagName('a') as $a) {
                                $sku = $a->getAttribute('data-sku-id');
                                foreach ($DOM->getElementsByTagName('img') as $img) {
                                    //$colorImages[$img->getAttribute('title')] = $img->getAttribute('src');
                                    $listsValue[$l][$sku] = $img->getAttribute('title');
                                }
                            }
                        }

                        $result['listsName'] = $lists;
                        $result['listsValue'] = $listsValue;

                        foreach ($result['listsValue'] as $list) {
                            foreach ($list as $v) {
                                if ($v == '') {
                                    foreach ($DOM->getElementsByTagName('a') as $a) {
                                        $sku = $a->getAttribute('data-sku-id');
                                        foreach ($DOM->getElementsByTagName('span') as $span) {
                                            //$colorImages[$img->getAttribute('title')] = $img->getAttribute('src');
                                            $listsValue[$l][$sku] = $span->getAttribute('title');
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $pos = strpos($client, '[{"skuAttr');
        if($pos == null) {
            $noVarPos = strpos($client, '[{"skuPropIds');
            $lastPos = strrpos($client, 'var GaData');
            $val = $lastPos - $noVarPos;
            $product = substr($client, $noVarPos, $val);
            $product = str_replace(';', '', $product);
        }else {
            $pos = strpos($client, '[{"skuAttr');
            $lastPos = strrpos($client, 'var GaData');
            $val = $lastPos - $pos;
            $product = substr($client, $pos, $val);
            $product = str_replace(';', '', $product);
        }

        $product = json_decode($product, true);

        if(!$product) {
//            file_put_contents(Yii::getAlias('@console').'/runtime/ali_not_product_'.time().'.txt', $link.print_r($product, true));
            return false;
        }
        if (!empty($product[1])) {
            foreach ($product as $variant) {
                foreach ($localProduct->variants as $val) {
                    $rrr = $localProduct->UserID;

                    $variant_sku = $variant['skuPropIds'];

                    if(isset($variant['skuVal']['actSkuCalPrice'])){
                        $price = $variant['skuVal']['actSkuCalPrice'];
                    }else{
                        $price = $variant['skuVal']['skuCalPrice'];
                    }

                    if(isset($variant['skuVal']['availQuantity'])){
                        $quantity = $variant['skuVal']['availQuantity'];
                    }elseif (isset($variant['skuVal']['inventory'])){
                        $quantity = $variant['skuVal']['inventory'];
                    }else{
                        $quantity = 0;
                    }
                    if ($val->Price != $price && $val->SKU == $variant_sku && $quantity) {
                        $val->Price = $price;
                        $val->LastScanTime =  date('y/m/d H:i:s');
                    } else {
                        $val->LastScanTime =  date('y/m/d H:i:s');
                    }
                    if ($val->StockUnits != $quantity && $val->SKU == $variant_sku) {
                        $val->StockUnits = $quantity;
                        $val->LastScanTime =  date('y/m/d H:i:s');
                    }
                    $val['LastScanTime'] = date('y/m/d H:i:s');
                    $val->save();

                }
            }
        } else {
            foreach ($localProduct->variants as $val) {
                $rrr = $localProduct->UserID;
                if(isset($product[0]['skuVal']['actSkuCalPrice'])){
                    $price = $product[0]['skuVal']['actSkuCalPrice'];
                } else{
                    $price = $product[0]['skuVal']['skuCalPrice'];
                }


                if(isset($product[0]['skuVal']['availQuantity'])){
                    $quantity = $product[0]['skuVal']['availQuantity'];
                }elseif (isset($product[0]['skuVal']['inventory'])){
                    $quantity = $product[0]['skuVal']['inventory'];
                }else{
                    $quantity = 0;
                }


                if ($val->Price != $price && $quantity) {
                    $val->Price = $price;
                    $val->LastScanTime =  date('y/m/d H:i:s');
                } else {
                     $val['LastScanTime'] = date('y/m/d H:i:s');
                }

                if ($val->StockUnits != $quantity) {
                    $val->StockUnits = $quantity;
                    $val->LastScanTime =  date('y/m/d H:i:s');
                }
                $val['LastScanTime'] = date('y/m/d H:i:s');
                $val->save();
            }
        }
    }

    public function actionWalmart() {
        $lastLog = Log::find()->where(['process' => 'Walmart'])->orderBy('id DESC')->one();

        if($lastLog && !$lastLog->finished && (time()- $lastLog->started) < 3600) {
            return false;
        }
        $db_products = Products::find()->joinWith(['variants'])->andWhere(['Vendor' => 'walmart'])->asArray()->all();

        if(!$db_products)
            return false;

        $log = new Log();
        $log->process = 'Walmart';
        $log->started = time();
        $log->save();

        // Sending Curl Paralel requests to aliexpress
        $this->walProducts = Products::find()->where([
            'Vendor' => 'walmart',
        ])->with('variants')->indexBy('ProductUrl')->all();

        if (!file_exists('/home/ara100/app.inventorify.com/console/controllers/log')) {
            fopen('/home/ara100/app.inventorify.com/console/controllers/log', 'a');
            file_put_contents('/home/ara100/app.inventorify.com/console/controllers/log', 'ID' . "\t\t" . 'Product ID' . "\t\t\t" . 'Variant ID' . "\t\t\t" . 'User ID' . "\t\t" . 'URL' . "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" . 'Old values' . "\t" . 'New values' . "\n", FILE_APPEND);
        }

        $log->addInfo('walmart url loop');
        $links = array_keys($this->walProducts);
        $curl = new CurlHelper([$this, 'walFinished'], 10); // call aliFinished callback when one of requests is finished

        $this->log = $log;
        $walmartCount = 0;
        foreach ($links as $link) {
            $curl->requests($link);
            $log->productCount(++$walmartCount);
        }

        $log->addInfo('finish');

        $curl->execute();
        // finish requests



        // Starting shopify
        $db_products = Products::find()->joinWith(['variants'])->andWhere(['Vendor' => 'walmart'])->asArray()->all();
        $users_id = [];
        foreach ($db_products as $key => $db_product){
            $users_id[$key] = $db_product['UserID'];
        }

        $users_id = array_unique($users_id);

        $appSetting = new AppSettings();
        $res = $appSetting->find()->one();
        $apiKey = $res->api_key;
        $secret = $res->shared_secret;


        $log->addInfo('walmart user_id loop');

        foreach ($users_id as $id){
            $model = new UserSettings();
            $userSettings = $model->findOne($id);

            $token = $userSettings['access_token'];
            $shop = $userSettings['store_name'];
            $sc = new ShopifyClient($shop, $token, $apiKey, $secret);

            Variants::updateShopifyVariants($sc, $log, $id);
        }

        $log->addInfo('walmart user_id loop finish');

        $log->finished = time();
        $log->save();
    }

    public function walFinishedOld(RequestInterface $request)
    {

        $link = $request->getUrl();
        $code = $request->http_code;
        $client = $request->response;
        if($code == 429) {
            $f = fopen(Yii::getAlias('@console').'/controllers/crawlera_log.data', 'a');
            $message = "\n\n".date('Y-m-d H:i:s')." Status Code 429 \n
             - request - $link \n
             - response - $client \n\n
             ";
            fputs($f, $message);
            return false;
        }


        if(!is_string($client)) {
            return false;
        }

        $localProduct = ArrayHelper::getValue($this->walProducts, str_replace('http:', 'https:', $link));
        /* @var Products $localProduct */
        if(!$localProduct || !$client) {
            return false;
        }

        $noVarPos = strpos($client, '{"uuid"');
        $lastPos = strpos($client, ';};</script><title>');
        $val = $lastPos - $noVarPos;
        $res = substr($client, $noVarPos, $val);

        $stock_pos = strrpos($client, '{"availabilityStatus"');
        $stock_pos_last = strrpos($client, ',"fulfillment":{"has');
        $stock_val = $stock_pos_last - $stock_pos;
        $stock = substr($client, $stock_pos, $stock_val);

        $stock = json_decode($stock);

        $product = json_decode($res);

        $price = $product->product->midasContext->price;


        if($stock->availabilityStatus == 'IN_STOCK'){
            $stock = 'In stock';
        }else{
            $stock = 'Out of stock';
        }

        if ($product->product->products) {
            foreach ($product->product->products as $sku => $variant) {


                foreach ($localProduct->variants as $val) {
                    $rrr = $localProduct->UserID;

                    $variant_sku = $sku;

                    if ($val->Price != $price && $val->SKU == $variant_sku && $stock) {
                        $val->Price = $price;
                        $val->LastScanTime =  date('y/m/d H:i:s');
                    } else {
                        $val->LastScanTime =  date('y/m/d H:i:s');
                    }
                    if ($val->StockUnits != $stock && $val->SKU == $variant_sku) {
                        $val->StockUnits = $stock;
                        $val->LastScanTime =  date('y/m/d H:i:s');
                    }
                    $val['LastScanTime'] = date('y/m/d H:i:s');
                    $val->save();
                }
            }

        } else {

            foreach ($localProduct->variants as $val) {
                $rrr = $localProduct->UserID;

                if ($val->Price != $price && $stock) {
                    $val->Price = $price;
                    $val->LastScanTime =  date('y/m/d H:i:s');
                } else {
                     $val['LastScanTime'] = date('y/m/d H:i:s');
                }

                if ($val->StockUnits != $stock) {
                    $val->StockUnits = $stock;
                    $val->LastScanTime =  date('y/m/d H:i:s');
                }
                $val['LastScanTime'] = date('y/m/d H:i:s');
                $val->save();
            }
        }
    }

    public function walFinished(RequestInterface $request)
    {

        $link = $request->getUrl();
        $code = $request->http_code;
        $client = $request->response;
        if($code == 429) {
            $f = fopen(Yii::getAlias('@console').'/controllers/crawlera_log.data', 'a');
            $message = "\n\n".date('Y-m-d H:i:s')." Status Code 429 \n
             - request - $link \n
             - response - $client \n\n
             ";
            fputs($f, $message);
            return false;
        }


        if(!is_string($client)) {
            return false;
        }

        $localProduct = ArrayHelper::getValue($this->walProducts, str_replace('http:', 'https:', $link));
        /* @var Products $localProduct */
        if(!$localProduct || !$client) {
            return false;
        }

        $noVarPos = strpos($client, '{"uuid"');
        $lastPos = strpos($client, ';};</script><title>');
        $val = $lastPos - $noVarPos;
        $res = substr($client, $noVarPos, $val);

        $stock_pos = strrpos($client, '{"availabilityStatus"');
        $stock_pos_last = strrpos($client, ',"fulfillment":{"has');
        $stock_val = $stock_pos_last - $stock_pos;
        $stock = substr($client, $stock_pos, $stock_val);

        $stock = json_decode($stock);

        $data = json_decode($res);


        if(empty($data)) {
            return  false;
        }
        $ProductId =  $data->productBasicInfo->selectedProductId;
        if(!$ProductId) {
            return false;
        }

        if(empty((array)$data->product->variantCategoriesMap)) {
            $offersId = $data->product->products->$ProductId->offers[0];
            $offers = $data->product->offers;
            if($offersId) {
                $offers = $offers->$offersId;
            } else {
                file_put_contents(Yii::getAlias('@console').'/runtime/nullOffersID_'.time().'.text', print_r($res, true));
            }

            if(!$offers) {
                return false;
            }
            //Quantity
            $quantity = $offers->productAvailability->availabilityStatus;
            if($quantity == 'IN_STOCK'){
                $stock = 'In stock';
            }else{
                $stock = 'Out of stock';
            }

            //price
            $priceData = $offers->pricesInfo->priceMap;
            if(isset($priceData->CURRENT)) {
                $price = $priceData->CURRENT->price;
            } else {
                $price = $priceData->WAS->price;
            }
            if(empty($price)) {
                $price = 0;
            }

            foreach ($localProduct->variants as $val) {
                $rrr = $localProduct->UserID;

                if ($val->Price != $price && $stock) {
                    $val->Price = $price;
                    $val->LastScanTime =  date('y/m/d H:i:s');
                } else {
                     $val['LastScanTime'] = date('y/m/d H:i:s');
                }

                if ($val->StockUnits != $stock) {
                    $val->StockUnits = $stock;
                    $val->LastScanTime =  date('y/m/d H:i:s');
                }
                $val['LastScanTime'] = date('y/m/d H:i:s');
                $val->save();
            }

            return  [
                'Price' => $price,
                'Quantity' => $quantity

            ];
        }

        foreach ($data->product->products as $sku => $variant) {
            $offersId = $variant->offers[0];
            $offers = $data->product->offers;
            if(empty($offers->$offersId)) {
                continue;
            }
            $offers = $offers->$offersId;

            //Quantity
            $quantity = $offers->productAvailability->availabilityStatus;
            if($quantity == 'IN_STOCK'){
                $stock = 'In stock';
            }else{
                $stock = 'Out of stock';
            }

            //price
            $priceData = $offers->pricesInfo->priceMap;
            if (isset($priceData->CURRENT)) {
                $price = $priceData->CURRENT->price;
            } else {
                $price = $priceData->LIST->price;
            }
            if (empty($price)) {
                $price = 0;
            }
            foreach ($localProduct->variants as $val) {

                $rrr = $localProduct->UserID;

                $variant_sku = $sku;
                if ($val->Price != $price && $val->SKU == $variant_sku && $stock) {

                    $val->Price = $price;
                    $val->LastScanTime =  date('y/m/d H:i:s');
                } else {
                    $val->LastScanTime =  date('y/m/d H:i:s');
                }
                if ($val->StockUnits != $stock && $val->SKU == $variant_sku) {
                    $val->StockUnits = $stock;
                    $val->LastScanTime = date('y/m/d H:i:s');
                }
                $val['LastScanTime'] = date('y/m/d H:i:s');
               $val->save();

            }
        }
    }

    public function actionRunUpdate($id = null) {
        if(!$id) return false;
        $runUpdate = RunUpdate::findOne($id);
        $runUpdate->delete();
        if(!$runUpdate) return false;
        $variants = Variants::find()->where(['IN', 'ID', json_decode($runUpdate->data, true)])->all();
        Variants::updateShopifyVariantsNew($variants,  $runUpdate->user_id);
        return true;
    }

    public function actionAutoResponder() {
        $listId = MailChimpClient::PENDING_LIST;
        $users = UserSettings::find()->where(['sideList' => 0, 'status' => 'active'])->all();
        foreach ($users as $user) {
            /* @var UserSettings $user*/
            if($user->status == 'active') {
                try {
                    $user->addToList(MailChimpClient::ACTIVE_LIST);
                    $user->sideList = 1;
                    $user->save();
                }
                catch (ClientException $ex){}
                continue;
            }
       /*     try {
                $user->addToList($listId);
                $user->sideList = 1;
                $user->save();
            }
            catch (ClientException $ex){
                $f = fopen(Yii::getAlias('@frontend').'/runtime/errors.logs', 'a');
                fputs($f, date('Y-m-d H:i:s').' - '.$ex->getMessage()."\n");
            }*/
        }
    }

    public function actionUninstallUserResponder($userSettings = null) {
        $shop = 'babydealshop.myshopify.com';
        $userSettings = UserSettings::find()->where(['store_name'=>$shop])->one();
        if(!$userSettings) return false;
        $listId = 'd95ce5360c';
        $settings = AppSettings::find()->one();
            if($userSettings->status != 'active') {
                try {
                    $shopifyApi = new ShopifyApi(
                        $settings->api_key,
                        $userSettings->access_token,
                        $userSettings->store_name,
                        new \GuzzleHttp\Client(['verify' => false])
                    );
                    $details = $shopifyApi->getShop();//gets this specific shop details from shopify
                    $myJSONString = json_encode($details['shop']); //toString
                    $myArray = json_decode($myJSONString); //toArray
                    $plan = $userSettings->plan;
                    if (is_null($userSettings->plan)) {
                        $plan = -1;
                    }
                    $plan_name_short = AddProductForm::getPlanName($plan);
                    $plan_name_short = $plan_name_short . ' PLan';
                    $data = [
                        'email' => $myArray->email,
                        'status' => 'subscribed',
                        'fullName' => $myArray->name,
                        'shopName' => $myArray->shop_owner,
                        'plan' => $plan_name_short
                    ];
                    $mailChimpClient = new MailChimpClient();
                    $status = $mailChimpClient->addSubscriberToList($listId, $data);
                } catch (ClientException $e) {
                } catch (ServerException $e) {
                }
            }
    }

    public function actionVariantLog() {
        Products::findAll(['UserID' => 77]);
        $variants = Variants::find()->where(['ProductID' => Products::find()->select('ID')->where(['UserID' => 77])])
            ->andWhere(['toBeSynced' => 1])
            ->all();


        Variants::updateShopifyVariantsTest($variants, 77);

    }

    public function actionAddWebhooks($id) {
        if(is_numeric($id)) {
            $user = UserSettings::findOne($id);
            $user->addWebhooksInstall();
        }
    }

    public function actionChangeAmazonUrl() {
        echo '<pre>';
        echo "count all product=> ".Products::find()->where(['Vendor' => 'amazon'])->count();
        foreach (Products::find()->where(['Vendor' => 'amazon'])->batch(100) as $products) {
            /** @var Products $product */
            foreach ($products as $product) {
                $variantSKU = $product->getVariants()->select('SKU')->limit(1)->one();
                $id = ArrayHelper::getValue($variantSKU, 'SKU');
                $productAPIParent = [];
                $url = $product->ProductUrl;
                if(!$id) {
                    $id = Log::getParceUrl('amazon',$url);
                }

                $region = Log::getParceUrl('amazon',$url, true);
                $productAmazon = new AmazonECS("", "", $region);

                echo 'DB=> '. $id,"\n",$url,"\n";

                try{
                    $productAPI = $productAmazon->lookup($id);
                }catch (\Exception $e){
                    sleep(2);
                    $productAPI = @$productAmazon->lookup($id);
                }
                if (isset($productAPI->Items->Item->ParentASIN)) {
                    if ($productAPI->Items->Item->ASIN != $productAPI->Items->Item->ParentASIN) {
                        try{
                            $productAPIParent = $productAmazon->lookup($productAPI->Items->Item->ParentASIN);
                        }catch (\Exception $e){
                            sleep(2);
                            $productAPIParent = $productAmazon->lookup($productAPI->Items->Item->ParentASIN);
                        }
                    }
                }
                if($productAPIParent &&  ($urlApi = $productAPIParent->Items->Item->DetailPageURL)) {
                   echo "Api=>"; print_r($url); echo "\n";
                    print_r($urlApi);
                    echo "\n\n";
                    $product->ProductUrl = $urlApi;
                    $product->save(false);
                }
            }
        }
    }


    public function actionUpdateVariantsPrice() {
        $settings = AppSettings::find()->one();
        foreach (Products::find()->batch(100) as $products) {
            /** @var Products $product*/
            foreach ($products as $product) {
                if(!$product->variantsNotSKU) {
                    continue;
                }
                $shopifyApi = new ShopifyApi(
                    $settings->api_key,
                    $product->user->access_token,
                    $product->user->store_name,
                    new \GuzzleHttp\Client(['verify' => false])
                );
                try {
                    $shopifyProductsVariants = ArrayHelper::getValue($shopifyApi->getProductVariants($product->ShopifyProductID), 'variants');
                    if(!$shopifyProductsVariants) {
                        continue;
                    }
                    $shopifyProductsVariants = ArrayHelper::map($shopifyProductsVariants, 'id', 'price');
                    /** @var Variants $variant */
                    foreach ($product->variantsNotSKU as $variant) {
                        if(!$variant->ShopifyID) {
                            continue;
                        }
                        if($shopifyVariantPrice = ArrayHelper::getValue($shopifyProductsVariants, $variant->ShopifyID)) {
                            echo "\n", $variant->Price, "\n", $shopifyVariantPrice, "\n";
                            $variant->Price = $shopifyVariantPrice;
                            $variant->save(false);
                        }
                    }
                } catch (ClientException $e) {
                    print_r($e->getMessage());
                    continue;
                }
            }
        }
    }

    public function actionTest() {
        return false;
        $errors = [];
        foreach (UserSettings::find()->where(['status' => 'active'])->batch(100) as $users) {
            foreach ($users as $user) {
                echo "try $user->id\n";
                /** @var UserSettings $user */
                $webhooks = $user->shopifyApi()->getWebhooks();
                if($error = ArrayHelper::getValue($webhooks,' error')) {
                    echo "error $user->id\n";
                    $errors[$user->id] = $error;
                    continue;
                }
                $webhooks = ArrayHelper::getValue($webhooks, 'webhooks');
                $webhooks =ArrayHelper::map($webhooks, 'id', 'address');
                foreach ($webhooks as $id => $address) {
                    if(strpos($address, 'http://') !== false) {
                        echo "catch $user->id\n";
                        $user->shopifyApi()->updateWebhooks($id, str_replace('http://', 'https://', $address));
                    }
                }
            }
        }
        file_put_contents('/home/ara100/app.inventorify.com/ttErrors.json', json_encode($errors));
    }
}